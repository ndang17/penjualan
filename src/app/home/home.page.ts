import { Component } from '@angular/core';
import { Platform, AlertController, NavController, LoadingController, ToastController } from '@ionic/angular';

import { Router } from '@angular/router';

import { ApiService } from 'src/app/_services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [ApiService]
})
export class HomePage {

  subscribe: any
  listProducts: any
  total_check_out = 0;

  constructor(
    private apiService: ApiService,
    public NavController: NavController,
    private router: Router
  ) { }

  ionViewDidEnter() {
    this.loadProducts();

    let keranjang = localStorage.getItem('keranjang');
    if (keranjang != '' && keranjang != null && keranjang != 'null') {
      let dataKeranjang = JSON.parse(keranjang);
      this.total_check_out = dataKeranjang.length;
    }
  }

  loadProducts() {

    this.listProducts = [];

    this.subscribe = this.apiService.get('penjualan/products', {}).subscribe(
      response => {
        console.log(response);
        this.listProducts = response.data;
      },
      error => {
        // this.apiService.presentToast('danger', 'Error', 'Data tidak tersimpan');
      }
    );

  }

  gotoDetail(menu: any) {
    let navigationExtras: any = {
      queryParams: {
        special: JSON.stringify(menu)
      }
    };
    this.router.navigate(['/produkdetails'], navigationExtras);
  }

  actionBuy(menu: any) {
    console.log('menu', menu);
    menu["quantity"] = 1;

    let keranjang = localStorage.getItem('keranjang');
    let dataNewKeranjang = [];
    if (keranjang != '' && keranjang != null && keranjang != 'null') {
      let dataKerjanjang = JSON.parse(keranjang);
      if (dataKerjanjang.length > 0) {
        let addToKeranjang = true;
        for (let i = 0; i < dataKerjanjang.length; i++) {
          const element = dataKerjanjang[i];
          if (element.product_id == menu.product_id) {
            addToKeranjang = false;
          }

          dataNewKeranjang.push(element);

        }

        if (addToKeranjang) {
          dataNewKeranjang.push(menu);
          this.apiService.presentToast('success', 'Sukses', 'Login Berhasil');
        } else {
          this.apiService.presentToast('warning', 'Warning', 'Sudah ada dalam keranjang');
        }
      } else {
        dataNewKeranjang.push(menu);
        this.apiService.presentToast('success', 'Sukses', 'Login Berhasil');
      }
    } else {
      dataNewKeranjang.push(menu);
      this.apiService.presentToast('success', 'Sukses', 'Login Berhasil');
    }

    // console.log('dataNewKeranjang', dataNewKeranjang);

    localStorage.setItem('keranjang', JSON.stringify(dataNewKeranjang));

    this.total_check_out = dataNewKeranjang.length;

  }


}
