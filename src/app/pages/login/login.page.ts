import { Component, OnInit } from '@angular/core';
import { Platform, AlertController, NavController, LoadingController, ToastController } from '@ionic/angular';

import { ApiService } from 'src/app/_services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  providers: [ApiService]
})
export class LoginPage implements OnInit {

  user: any
  password: any

  subscribe: any

  constructor(
    private apiService: ApiService,
    public NavController: NavController,
  ) {
    this.user = 'User';
    this.password = 'Password';
  }

  ngOnInit() {
  }

  loginNow() {
    let params = {
      user: this.user,
      password: this.password,
    };

    this.subscribe = this.apiService.post('penjualan/login', params).subscribe(
      response => {
        console.log(response);
        // this.loadingbutton = false;
        if (response.status) {
          localStorage.setItem('user', JSON.stringify(response.data));
          this.apiService.presentToast('success', 'Sukses', 'Login Berhasil');
          this.NavController.navigateForward(['/home'], { replaceUrl: true });
        } else {
          this.apiService.presentToast('danger', 'Error', 'User dan Password tidak cocok');
        }



      },
      error => {
        this.apiService.presentToast('danger', 'Error', 'Data tidak tersimpan');
        // setTimeout(() => {
        //   this.loadingbutton = false;
        // }, 2000);
      }
    );

  }

}
