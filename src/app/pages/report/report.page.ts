import { Component, OnInit } from '@angular/core';
import { Platform, AlertController, NavController, LoadingController, ToastController } from '@ionic/angular';

import { Router } from '@angular/router';

import { ApiService } from 'src/app/_services/api.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
  providers: [ApiService]
})
export class ReportPage implements OnInit {

  subscribe: any
  listTrx: any

  constructor(
    private apiService: ApiService,
    public NavController: NavController,
    private router: Router) { }

  ngOnInit() {
    this.loadProducts();
  }

  ionViewDidEnter() {
    this.loadProducts();
  }

  loadProducts() {

    this.listTrx = [];

    this.subscribe = this.apiService.get('penjualan/transaksi', {}).subscribe(
      response => {
        console.log(response);
        this.listTrx = response.data;
      },
      error => {
        // this.apiService.presentToast('danger', 'Error', 'Data tidak tersimpan');
      }
    );

  }

  gotoDetail(item: any) {
    let navigationExtras: any = {
      queryParams: {
        special: JSON.stringify(item)
      }
    };
    this.router.navigate(['/reportdetail'], navigationExtras);
  }

}
