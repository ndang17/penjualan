import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReportdetailPage } from './reportdetail.page';

describe('ReportdetailPage', () => {
  let component: ReportdetailPage;
  let fixture: ComponentFixture<ReportdetailPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ReportdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
