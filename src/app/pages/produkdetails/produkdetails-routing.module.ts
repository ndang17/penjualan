import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProdukdetailsPage } from './produkdetails.page';

const routes: Routes = [
  {
    path: '',
    component: ProdukdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProdukdetailsPageRoutingModule {}
