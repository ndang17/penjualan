import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProdukdetailsPage } from './produkdetails.page';

describe('ProdukdetailsPage', () => {
  let component: ProdukdetailsPage;
  let fixture: ComponentFixture<ProdukdetailsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ProdukdetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
