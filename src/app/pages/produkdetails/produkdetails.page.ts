import { Component, OnInit } from '@angular/core';

import {
  ModalController, NavController, AlertController, ToastController
  , LoadingController
} from '@ionic/angular';


import { ActivatedRoute, Router } from '@angular/router';

import { ApiService } from 'src/app/_services/api.service';

@Component({
  selector: 'app-produkdetails',
  templateUrl: './produkdetails.page.html',
  styleUrls: ['./produkdetails.page.scss'],
  providers: [ApiService]
})
export class ProdukdetailsPage implements OnInit {

  data: any

  constructor(
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      console.log(params['special']);
      this.data = JSON.parse(params['special']);
      console.log(this.data);
    });
  }

  ngOnInit() {
  }

}
