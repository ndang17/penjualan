import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProdukdetailsPageRoutingModule } from './produkdetails-routing.module';

import { ProdukdetailsPage } from './produkdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProdukdetailsPageRoutingModule
  ],
  declarations: [ProdukdetailsPage]
})
export class ProdukdetailsPageModule {}
