import { Component, OnInit } from '@angular/core';
import { Platform, AlertController, NavController, LoadingController, ToastController } from '@ionic/angular';

import { ApiService } from 'src/app/_services/api.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
  providers: [ApiService]
})
export class CheckoutPage implements OnInit {

  dataKeranjang: any
  totalBelanja = 0

  subscribe: any

  constructor(
    private apiService: ApiService,
    public NavController: NavController,
  ) {
    this.dataKeranjang = [];
    let keranjang = localStorage.getItem('keranjang');
    if (keranjang != '' && keranjang != null && keranjang != 'null') {
      this.dataKeranjang = JSON.parse(keranjang);
      this.HitungtotalBelanja();
    }

  }

  ngOnInit() {

  }

  HitungtotalBelanja() {
    let totalBelanja = 0
    for (let i = 0; i < this.dataKeranjang.length; i++) {
      const element = this.dataKeranjang[i];
      totalBelanja = totalBelanja + (element.quantity * element.price_after_discount);
    }
    this.totalBelanja = totalBelanja;
  }

  onChangeTime(ev: any, product_id: any) {
    let val = ev.target.value;
    // console.log('val', val);
    // console.log('product_id', product_id);

    let keranjang = localStorage.getItem('keranjang');
    if (keranjang != '' && keranjang != null && keranjang != 'null') {
      let dataKeranjang = JSON.parse(keranjang);
      let newDataKeranjang = [];

      for (let i = 0; i < dataKeranjang.length; i++) {
        const element = dataKeranjang[i];
        if (element.product_id == product_id) {
          element.quantity = val;
        }



        newDataKeranjang.push(element);
      }

      localStorage.setItem('keranjang', JSON.stringify(newDataKeranjang));
      this.dataKeranjang = newDataKeranjang;
      this.HitungtotalBelanja();
    }

  }

  formatRupiah(angka: any, prefix = 'Rp. ') {
    angka = '' + angka;
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    let separator = '';
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }

  confirmPesanan() {
    let params = {
      data_user: localStorage.getItem('user'),
      keranjang: localStorage.getItem('keranjang'),
    };

    this.subscribe = this.apiService.post('penjualan/transaksi', params).subscribe(
      response => {
        console.log(response);
        // this.loadingbutton = false;
        if (response.status) {
          localStorage.setItem('user', JSON.stringify(response.data));
          this.apiService.presentToast('success', 'Sukses', 'Pesanan Berhasil');
          localStorage.setItem('keranjang', JSON.stringify([])),
            this.NavController.navigateForward(['/home'], { replaceUrl: true });
        } else {
          this.apiService.presentToast('danger', 'Error', 'Pesanan Gagal');
        }



      },
      error => {
        this.apiService.presentToast('danger', 'Error', 'Data tidak tersimpan');
        // setTimeout(() => {
        //   this.loadingbutton = false;
        // }, 2000);
      }
    );
  }

}
