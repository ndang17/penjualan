import { Injectable } from '@angular/core';
import { AlertController, NavController, LoadingController, ToastController } from '@ionic/angular';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { Platform } from '@ionic/angular';
// import { error } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  loadingPresent: any
  loading: any

  subscribe: any

  constructor(
    private http: HttpClient,
    private _platform: Platform,
    public toastController: ToastController,
    private alertController: AlertController,
    public loadingController: LoadingController,
    public NavController: NavController,
  ) { }

  setHeader() {
    // let apiKey = localStorage.getItem('apiKey');

    var headerOption = {
      'accept-language': 'id',
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-API-KEY': 's8nhpV2ZLv1WtSWpNALk627wsvXkyOsudipXA4Bxxn6'
    }

    // if (apiKey != null && apiKey !== "undefined" && apiKey != '') {
    //   headerOption['X-API-KEY'] = apiKey;
    // }

    return new HttpHeaders(headerOption);
  }

  find(endpoint: string, params: any) {
    return this.http.get<{ results: [] }>(environment.apiUrl + endpoint,
      { headers: this.setHeader(), params: params });
  }

  get(endpoint: string, params: any) {
    return this.http.get<any>(environment.apiUrl + endpoint,
      { headers: this.setHeader(), params: params });
  }

  post(endpoint: string, params: any) {

    let body = this.genrateParam(params);

    return this.http.post<any>(`${environment.apiUrl + endpoint}`, body,
      { headers: this.setHeader() })
      .pipe(map(response => {
        return response;
      }));

  }


  put(endpoint: string, params: any) {

    let body = this.genrateParam(params);

    return this.http.put<any>(`${environment.apiUrl + endpoint}`, body,
      { headers: this.setHeader() })
      .pipe(map(response => {
        return response;
      }));

  }

  patch(endpoint: string, params: any) {

    let body = this.genrateParam(params);

    return this.http.patch<any>(`${environment.apiUrl + endpoint}`, body,
      { headers: this.setHeader() })
      .pipe(map(response => {
        return response;
      }));

  }

  delete(endpoint: string, params: any) {

    let body = this.genrateParam(params);

    return this.http.delete<any>(`${environment.apiUrl + endpoint}`, { headers: this.setHeader(), params: params })
      .pipe(map(response => {
        return response;
      }));

  }

  private genrateParam(params: any) {

    let paramsKey = Object.keys(params);
    let body = '';
    for (let i = 0; i < paramsKey.length; i++) {
      let spr = (i == 0) ? '' : '&';
      body = body + spr + paramsKey[i] + '=' + params[paramsKey[i]];
    }

    return body;
  }

  async presentToast(color: string, header: string, message: string) {
    const toast = await this.toastController.create({
      header: header, //"Warning",
      message: message, //"Mohon isi semua formulir",
      color: color, //"danger",
      mode: "ios",
      position: "middle",
      duration: 3000
    });
    toast.present();
  }

  async presentLoading() {
    this.loadingPresent = true;
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Loading...',
      mode: "ios",
      duration: 2000
    });
    await this.loading.present();

  }

  async setResetPassword() {
    const alert = await this.alertController.create({
      header: 'Lupa Password',
      subHeader: 'Untuk mereset password, Anda akan diarahkan ke whatsapp admin.',
      mode: 'ios',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            // this.handlerMessage = 'Alert canceled';
          },
        },
        {
          text: 'Kirim',
          role: 'confirm',
          handler: (data) => {
            let currentUser: any;
            currentUser = localStorage.getItem('currentUser');
            currentUser = JSON.parse(currentUser);
            let strg = 'Halo Admin, saya ingin mereset password akun saya, Akun saya : ' + currentUser.name + ' (' + currentUser.email + ')';
            strg.replace(' ', '%20');
            window.open('https://wa.me/6282198228626?text=' + strg);
          },
        },
      ],
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
  }

  async setGPin() {
    const alert = await this.alertController.create({
      header: 'Seting PIN',
      mode: 'ios',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            // this.handlerMessage = 'Alert canceled';
          },
        },
        {
          text: 'Kirim',
          role: 'confirm',
          handler: (data) => {

            if (data.pin.length != 4) {
              this.presentToast('danger', 'Warning', 'PIN harus kombinasi 4 angka');
            } else {
              let params = {
                password: data.password,
                pin: data.pin,
              };

              this.subscribe = this.post('reminder/login/pin', params).subscribe(
                response => {

                  if (response.status == true) {
                    this.presentToast('success', 'Sukses', 'PIN Tersimpan');

                    setTimeout(() => {
                      window.location.reload();
                    }, 2000);

                  } else {
                    this.presentToast('danger', 'Error 1', 'PIN tidak tersimpan');
                  }

                },
                error => {
                  setTimeout(() => {
                    this.presentToast('danger', 'Error 2', 'PIN tidak tersimpan');
                  }, 2000);
                }
              );
            }



          },
        },
      ],
      inputs: [
        {
          placeholder: 'Password Akun',
          type: 'password',
          name: 'password',
          // value: 'admin'
        },
        {
          placeholder: 'PIN Baru ( 4 angka )',
          type: 'number',
          name: 'pin',
          // value: 1234
        }
      ]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
  }

  async surveyKebahagiaan() {
    const alert = await this.alertController.create({
      header: 'Apakah Anda bahagia ?',
      backdropDismiss: false,
      mode: 'ios',
      buttons: [{
        text: 'Kirim',
        role: 'confirm',
        handler: (data) => {
          console.log('data', data);

          let rate = parseInt(data);

          let params = {
            kepuasan_value: rate
          };

          this.subscribe = this.post('reminder/survey/setsurvey', params).subscribe(
            data => {
              // console.log(data);
              this.presentToast('success', 'Sukses', 'Klaim Berhasil Terkirim');

            },
            error => {
              setTimeout(() => {
              }, 2000);
            }
          );

        }
      }],
      inputs: [
        {
          label: 'Sangat Bahagia',
          type: 'radio',
          value: '5',
          checked: true
        },
        {
          label: 'Merasa Puas',
          type: 'radio',
          value: '4',
        },
        {
          label: 'Merasa Bahagia',
          type: 'radio',
          value: '3',
        },
        {
          label: 'Kurang Bahagia',
          type: 'radio',
          value: '2',
        },
        {
          label: 'Tidak Bahagia',
          type: 'radio',
          value: '1',
        },
      ],
    });

    await alert.present();
  }

}
