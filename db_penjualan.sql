-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 18 Jun 2023 pada 05.02
-- Versi server: 10.2.44-MariaDB-cll-lve
-- Versi PHP: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medj5616_reminder`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `user_id` bigint(20) NOT NULL,
  `user` text DEFAULT NULL,
  `password` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`user_id`, `user`, `password`) VALUES
(1, 'User', 'Password');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `product_id` bigint(20) NOT NULL,
  `product_code` text DEFAULT NULL,
  `name` text DEFAULT NULL,
  `price` float DEFAULT NULL,
  `currency` enum('IDR') NOT NULL DEFAULT 'IDR',
  `discount` float DEFAULT 0,
  `dimension` text DEFAULT NULL,
  `unit` enum('PCS') NOT NULL DEFAULT 'PCS'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`product_id`, `product_code`, `name`, `price`, `currency`, `discount`, `dimension`, `unit`) VALUES
(1, 'SKUSKILNP', 'So klin Pewangi', 15000, 'IDR', 10, '13 cm x 10 cm', 'PCS'),
(2, 'DVBR', 'Giv Biru', 11000, 'IDR', 0, '5 cm', 'PCS'),
(3, 'SKPI', 'So Klin Pewangi', 18000, 'IDR', 0, '10 cm', 'PCS'),
(4, 'GVKN', 'Giv Kuning', 10000, 'IDR', 0, '5 cm', 'PCS');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction_headers`
--

CREATE TABLE `transaction_headers` (
  `transaction_header_id` bigint(20) NOT NULL,
  `document_code` varchar(3) DEFAULT NULL,
  `document_number` varchar(10) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `total` float DEFAULT NULL,
  `trx_date` datetime DEFAULT current_timestamp() COMMENT 'tanggal transaksi'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaction_headers`
--

INSERT INTO `transaction_headers` (`transaction_header_id`, `document_code`, `document_number`, `user_id`, `total`, `trx_date`) VALUES
(1, 'TRX', '0001', 1, 11000, '2023-06-18 04:00:12'),
(2, 'TRX', '0002', 1, 61000, '2023-06-18 04:37:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction_header_details`
--

CREATE TABLE `transaction_header_details` (
  `transaction_header_detail_id` bigint(20) NOT NULL,
  `transaction_header_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `product_code` varchar(18) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `unit` enum('PCS') DEFAULT 'PCS',
  `currency` enum('IDR') DEFAULT 'IDR',
  `quantity` int(11) DEFAULT NULL,
  `sub_total` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaction_header_details`
--

INSERT INTO `transaction_header_details` (`transaction_header_detail_id`, `transaction_header_id`, `product_id`, `product_code`, `price`, `unit`, `currency`, `quantity`, `sub_total`) VALUES
(1, 0, 2, 'DVBR', 11000, 'PCS', 'IDR', 1, 11000),
(2, 1, 2, 'DVBR', 11000, 'PCS', 'IDR', 1, 11000),
(3, 2, 2, 'DVBR', 11000, 'PCS', 'IDR', 1, 11000),
(4, 2, 4, 'GVKN', 10000, 'PCS', 'IDR', 5, 50000);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`user_id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indeks untuk tabel `transaction_headers`
--
ALTER TABLE `transaction_headers`
  ADD PRIMARY KEY (`transaction_header_id`);

--
-- Indeks untuk tabel `transaction_header_details`
--
ALTER TABLE `transaction_header_details`
  ADD PRIMARY KEY (`transaction_header_detail_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `login`
--
ALTER TABLE `login`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `product_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `transaction_headers`
--
ALTER TABLE `transaction_headers`
  MODIFY `transaction_header_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `transaction_header_details`
--
ALTER TABLE `transaction_header_details`
  MODIFY `transaction_header_detail_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
